const PATH = require('path')
const fs = require('fs')
const HTMLWebpackPlugin = require('html-webpack-plugin')
const LiveReloadPlugin = require('webpack-livereload-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin')
const TerserWebpackPlugin = require('terser-webpack-plugin')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
const IS_DEV = process.env.NODE_ENV === 'development'
const THEME_NAME = '[THEMENAME]'
const PATHS = {
	pug: PATH.resolve(__dirname, 'src/pug/pages/'),
	scss: PATH.resolve(__dirname, 'src/scss'),
	build: PATH.resolve(__dirname, `dist/wp-content/themes/${THEME_NAME}`)
}
const PUG_PAGES = fs.readdirSync(PATHS.pug).filter(fileName => fileName.endsWith('.pug'))
const setFilename = (name, ext) => `${name}.${ext}`
const fileOptimization = () => {
	const CONFIG = {
		splitChunks: {
			chunks: 'all'
		}
	}

	if (!IS_DEV) {
		CONFIG.minimizer = [
			new OptimizeCssAssetsWebpackPlugin(),
			new TerserWebpackPlugin()
		]
	}

	return CONFIG
}
const applyCssLoaders = loader => {
	const LOADERS = [{
		loader: MiniCssExtractPlugin.loader,
		options: {
			hmr: IS_DEV
		}
	}, 'css-loader']

	if (loader) LOADERS.push(loader)

	return LOADERS
}
const setBabelOptions = preset => {
	const OPTIONS = {
		presets: [
			'@babel/preset-env'
		],

		plugins: [
			'@babel/plugin-proposal-class-properties'
		]
	}

	if (preset) OPTIONS.presets.push(preset)

	return OPTIONS
}
const setJsLoaders = () => {
	const LOADERS = [
		{
			loader: 'babel-loader',
			options: setBabelOptions()
		}
	]

	if (IS_DEV) LOADERS.push('eslint-loader')

	return LOADERS
}
const addPlugins = () => {
	const BASE_PLUGINS = [
		...PUG_PAGES.map(page => new HTMLWebpackPlugin({
      template: `${PATHS.pug}/${page}`,
      filename: `./${page.replace(/\.pug/,'.html')}`
    })),

		new CleanWebpackPlugin(),

		new LiveReloadPlugin({
			appendScriptTag: true
		}),

		new CopyWebpackPlugin({
			patterns: [
				{
					from: PATH.resolve(__dirname, 'src/static'),
					to: PATH.resolve(__dirname, `${PATHS.build}/static`)
				}
			]
		}),

		new MiniCssExtractPlugin({
			filename: setFilename('style', 'css')
		})
	]

	if (!IS_DEV) BASE_PLUGINS.push(new BundleAnalyzerPlugin())

	return BASE_PLUGINS
}

module.exports = {
	context: PATH.resolve(__dirname, 'src'),

	entry: {
		app: ['@babel/polyfill', './index.js']
	},

	output: {
		filename: setFilename('bundle', 'js'),
		path: PATH.resolve(__dirname, `dist`)
	},

	resolve: {
		alias: {
			'@': PATH.resolve(__dirname, 'src'),
			'@scss': PATHS.scss
		}
	},

	optimization: fileOptimization(),

	devServer: {
		port: 4200,
		hot: IS_DEV
	},

	devtool: IS_DEV ? 'source-map' : '',

	mode: 'development',

	plugins: addPlugins(),

	module: {
		rules: [
			{
				test: /\.pug$/,
				use: ['pug-loader']
			},

			{
				test: /\.css$/,
				use: applyCssLoaders()
			},

			{
				test: /\.s[ac]ss$/,
				use: applyCssLoaders('sass-loader')
			},

			{
				test: /\.(png|jpeg|jpg|svg|gif|webp)$/,
				use: ['file-loader']
			},

			{
				test: /\.(ttf|woff|woff2)$/,
				use: ['file-loader']
			},

			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: setJsLoaders()
			}
		]
	}
}