# Webpack WordPress Template

#### Set themename

```
./webpack.config.js (const THEME_NAME)
./src/pug/utils/mixins (var theme)
Update "name", "description" and "keywords" in package.json
```

#### Install JavaScript packages

```
npm install
```

#### Run watch

```
npm run watch
```

#### Run dev

```
npm run dev
```

#### Run build

```
npm run build
```